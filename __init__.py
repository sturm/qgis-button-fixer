# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ButtonFixer
                                 A QGIS plugin
 Removes some buttons that confusing to new users.
                             -------------------
        begin                : 2018-12-06
        copyright            : (C) 2018 by Ben Sturmfels
        email                : ben@sturm.com.au
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load ButtonFixer class from file ButtonFixer.

    :param iface: A QGIS interface instance.
    :type iface: QgisInterface
    """
    #
    from .button_fixer import ButtonFixer
    return ButtonFixer(iface)
