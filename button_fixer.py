r"""
ButtonFixer

Removes some buttons that confusing to new users.

To install, open "Plugins", "Manage and Install Plugins", "Settings" and select
"Enable experimental plugins". Then in "Manage and Install Plugins", choose
"Install From ZIP". Choose "ButtonFixer.zip" and then "Install Plugin".

You can also copy straight to the plugins directory. In Windows that is:

  C:\Users\USER\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins

where "AppData" is a hidden directory.

In GNU/Linux that is:

  ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins

begin: 2018-12-06
git sha: $Format:%H$
copyright: (C) 2018 by Ben Sturmfels
email: ben@sturm.com.au

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

"""

# QGIS3 uses Python 3

# Raw string required above due to \U

from qgis.PyQt.QtWidgets import QDockWidget
# Initialize Qt resources from file resources.py
# import resources
# Import the code for the dialog
# from button_fixer_dialog import ButtonFixerDialog


def change_buttons(iface, visible=False):
    # Some useful examples:
    # https://gis.stackexchange.com/questions/229363/removing-buttons-from-qgis-layers-panel
    # https://gis.stackexchange.com/questions/228670/name-of-children-of-toolbar-in-qgis-python-and-how-to-disable
    # https://qgis.org/api/classQgisInterface.html#a713fa6cf8ece68e78b61c7e4d7ebfa16

    # Remove layer buttons.
    #
    # These buttons have a blank .objectName(), and .toolTip() so only chice is
    # to use index. They seem to *have* tool-tips in the app though, so not sure
    # why they're blank.
    # LAYER_BUTTONS_TO_REMOVE = [
    #     "Manage Map Themes",
    #     "<b>Filter Legend By Map Content</b>",
    #     "Filter Legend By Expression",
    # ]
    LAYER_BUTTONS_TO_REMOVE = [1, 2, 4, 5, 6, 7]
    layer_panel = iface.mainWindow().findChild(QDockWidget, "Layers")
    layer_buttons = layer_panel.children()[4].children()[1].actions()
    for btn in LAYER_BUTTONS_TO_REMOVE:
        layer_buttons[btn].setVisible(visible)

    # Remove attribute toolbar buttons.
    #
    # These buttons have a .objectName(), but .toolTip() is easier to guess from
    # the interface.
    ATTRIBUTES_BUTTONS_TO_REMOVE = [
        "<b>Open Field Calculator</b>",
        "<b>Show statistical summary</b>", # Case inconsistency
    ]
    attributes_toolbar = iface.attributesToolBar()
    attributes_buttons = attributes_toolbar.actions() # Why "actions" and not "children"?
    for btn in attributes_buttons:
        if getattr(btn, "toolTip", None) and btn.toolTip() in ATTRIBUTES_BUTTONS_TO_REMOVE:
            btn.setVisible(visible)
            print("Removed '{}'".format(btn.toolTip()))
        elif getattr(btn, "toolTip", None):
            print("Ignored '{}'".format(btn.toolTip()))


class ButtonFixer:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgisInterface
        """
        print("Ran __init__()")
        self.iface = iface

    def initGui(self):
        print("Ran initGui()")
        change_buttons(self.iface, visible=False)

    def unload(self):
        print("Ran unload()")
        change_buttons(self.iface, visible=True)
